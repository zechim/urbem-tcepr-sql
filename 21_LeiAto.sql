﻿SELECT 
       (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       root_norma.cod_norma AS cdControleLeiAto, 
       (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoaOrigem,
       root_norma.cod_tipo_norma AS idTipoDocumento,
       (
         SELECT TRIM(valor.valor)
           FROM normas.norma norma
           JOIN normas.atributo_norma_valor valor ON (norma.cod_norma = valor.cod_norma)
           JOIN administracao.atributo_dinamico atributo ON (valor.cod_modulo = atributo.cod_modulo 
            AND valor.cod_cadastro = atributo.cod_cadastro 
            AND valor.cod_atributo = atributo.cod_atributo)
          WHERE norma.cod_norma = root_norma.cod_norma 
            AND LOWER(atributo.nom_atributo) LIKE 'escopo%' LIMIT 1
       ) AS idEscopo,
       (
         SELECT CASE WHEN valor.valor = '0' THEN '' ELSE TRIM(valor.valor) END
           FROM normas.norma norma
           JOIN normas.atributo_norma_valor valor ON (norma.cod_norma = valor.cod_norma)
           JOIN administracao.atributo_dinamico atributo ON (valor.cod_modulo = atributo.cod_modulo 
            AND valor.cod_cadastro = atributo.cod_cadastro 
            AND valor.cod_atributo = atributo.cod_atributo)
          WHERE norma.cod_norma = root_norma.cod_norma 
            AND atributo.nom_atributo = 'Número da Lei'
       ) AS nrLeiAto,
       TRIM(root_norma.exercicio) AS nrAnoLeiAto,
       TO_CHAR(root_norma.dt_assinatura, 'YYYY-MM-DD') AS dtLeiAto,
       (
         SELECT TRIM(valor.valor)
           FROM normas.norma norma
           JOIN normas.atributo_norma_valor valor ON (norma.cod_norma = valor.cod_norma)
           JOIN administracao.atributo_dinamico atributo ON (valor.cod_modulo = atributo.cod_modulo 
            AND valor.cod_cadastro = atributo.cod_cadastro 
            AND valor.cod_atributo = atributo.cod_atributo)
          WHERE norma.cod_norma = root_norma.cod_norma 
            AND atributo.nom_atributo = 'Código ATOTECA'
       ) AS cdControleDocumento,
       (
         SELECT TRIM(valor.valor)
           FROM normas.norma norma
           JOIN normas.atributo_norma_valor valor ON (norma.cod_norma = valor.cod_norma)
           JOIN administracao.atributo_dinamico atributo ON (valor.cod_modulo = atributo.cod_modulo 
            AND valor.cod_cadastro = atributo.cod_cadastro 
            AND valor.cod_atributo = atributo.cod_atributo)
          WHERE norma.cod_norma = root_norma.cod_norma 
            AND atributo.nom_atributo = 'Ano Inicial'
       ) AS nrAnoInicialAplicacao, 
       '' AS final
  FROM normas.norma root_norma
 WHERE TO_CHAR(TO_DATE((
         SELECT TRIM(valor.valor)
           FROM normas.norma norma
           JOIN normas.atributo_norma_valor valor ON (norma.cod_norma = valor.cod_norma)
           JOIN administracao.atributo_dinamico atributo ON (valor.cod_modulo = atributo.cod_modulo 
            AND valor.cod_cadastro = atributo.cod_cadastro 
            AND valor.cod_atributo = atributo.cod_atributo)
          WHERE norma.cod_norma = root_norma.cod_norma 
            AND atributo.nom_atributo = 'Data Referência TCE'
       ), 'DD/MM/YYYY'), 'YYYYMM') = 'PARAMETRO_DT_REFERENCIA_TCE_YYYYMM'
ORDER BY root_norma.cod_norma ASC
