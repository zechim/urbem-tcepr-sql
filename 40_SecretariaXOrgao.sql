﻿SELECT (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       secretaria.id_secretaria_tce AS idSecretaria,
       secretaria.cod_orgao AS cdOrgao,
       TRIM(secretaria.exercicio) AS nrAnoLOA,
       '' AS final
  FROM tcepr.secretaria_x_orgao AS secretaria
 WHERE TO_CHAR(secretaria.dt_cadastro, 'YYYYMM') = 'PARAMETRO_DT_CADASTRO_YYYYMM'
