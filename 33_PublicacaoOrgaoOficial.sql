﻿SELECT (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       norma_cod_norma AS cdControleLeiAto,
       TO_CHAR(norma_dt_publicacao, 'YYYY-MM-DD') AS dtPublicacao,
       orgao_oficial[orgao_oficial_index] AS cdOperacao,
       '' AS final
  FROM (
         SELECT generate_series(1, array_upper(orgao_oficial, 1)) AS orgao_oficial_index,
                orgao_oficial.orgao_oficial,
                orgao_oficial.norma_cod_norma,
                orgao_oficial.norma_dt_publicacao
           FROM (
                  SELECT orgao_oficial.norma_dt_publicacao,
                         orgao_oficial.norma_cod_norma,
                         orgao_oficial.orgao_oficial 
                    FROM (
                           SELECT norma.cod_norma AS norma_cod_norma,
                                  norma.dt_publicacao AS norma_dt_publicacao,
                                  regexp_split_to_array(CASE WHEN valor.valor = '0' THEN '' ELSE TRIM(valor.valor) END, ',') AS orgao_oficial
                             FROM normas.norma norma
                             JOIN normas.atributo_norma_valor valor ON (norma.cod_norma = valor.cod_norma)
                             JOIN administracao.atributo_dinamico atributo ON (valor.cod_modulo = atributo.cod_modulo 
                              AND valor.cod_cadastro = atributo.cod_cadastro 
                              AND valor.cod_atributo = atributo.cod_atributo)
                            WHERE atributo.nom_atributo = 'Órgão Oficial'
                        ) orgao_oficial
               ) orgao_oficial
       ) orgao_oficial
 WHERE TO_CHAR(norma_dt_publicacao, 'YYYYMM') = 'PARAMETRO_NORMA_DT_PUBLICACAO_YYYYMM'
 ORDER BY norma_cod_norma ASC

