﻿SELECT (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       mei.cod_indicador AS cdIndicador,
       mei.cod_norma_criacao AS cdControleLeiAto,
       iom.id_objetivo_milenio AS idObjetivoMilenio,
       '' AS final
  FROM tcepr.indicador_x_objetivo_milenio AS iom
  JOIN tcepr.medida_esperada_indicador mei ON (mei.cod_indicador = iom.cod_indicador 
   AND mei.cod_programa = iom.cod_programa 
   AND mei.timestamp_programa_dados = iom.timestamp_programa_dados)
  JOIN ppa.programa_indicadores indicador ON (indicador.cod_indicador = mei.cod_indicador 
   AND indicador.cod_programa = mei.cod_programa 
   AND indicador.timestamp_programa_dados = mei.timestamp_programa_dados)
  JOIN ppa.programa_dados dados ON (dados.cod_programa = indicador.cod_programa 
   AND dados.timestamp_programa_dados = indicador.timestamp_programa_dados)
  JOIN ppa.programa programa ON (programa.cod_programa = indicador.cod_programa)
  JOIN ppa.programa_setorial programa_setorial ON (programa_setorial.cod_setorial = programa.cod_setorial)
  JOIN ppa.macro_objetivo ON (macro_objetivo.cod_macro = programa_setorial.cod_macro)
  JOIN ppa.ppa_publicacao publicacao ON (publicacao.cod_ppa = macro_objetivo.cod_ppa)
  JOIN normas.norma norma ON (norma.cod_norma = publicacao.cod_norma)
 WHERE TO_CHAR(publicacao.timestamp, 'YYYYMM') = 'PARAMETRO_PUBLICACAO_YYYYMMM'
 GROUP BY mei.cod_indicador, mei.cod_norma_criacao, iom.id_objetivo_milenio
 ORDER BY mei.cod_indicador ASC
