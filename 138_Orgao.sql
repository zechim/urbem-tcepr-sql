﻿SELECT (
   SELECT TRIM(config.valor)
     FROM administracao.configuracao config
    WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
      AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       orgao.num_orgao AS cdOrgao,
       orgao.exercicio AS nrAnoLOA,
       orgao.nom_orgao AS nmOrgao,
       '' AS final
  FROM orcamento.orgao orgao
 ORDER BY orgao.num_orgao ASC
