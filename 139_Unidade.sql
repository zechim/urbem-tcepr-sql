﻿SELECT (
   SELECT TRIM(config.valor)
     FROM administracao.configuracao config
    WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
      AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       unidade.num_orgao AS cdOrgao,
       unidade.num_unidade AS cdUnidade,
       unidade.exercicio AS nrAnoLOA,
       unidade.nom_unidade AS nmUnidade,
       '' AS final
  FROM orcamento.unidade unidade
 ORDER BY unidade.num_unidade ASC
