﻿SELECT (
   SELECT TRIM(config.valor)
     FROM administracao.configuracao config
    WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
      AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       acao.cod_acao AS cdAcao,
       norma.cod_norma AS cdControleLeiAto,
       '' AS final
  FROM ppa.acao acao
  JOIN ppa.programa programa ON (programa.cod_programa = acao.cod_programa)
  JOIN ppa.programa_setorial programa_setorial ON (programa_setorial.cod_setorial = programa.cod_setorial)
  JOIN ppa.macro_objetivo macro_objetivo ON (macro_objetivo.cod_macro = programa_setorial.cod_macro)
  JOIN ppa.ppa ppa ON (ppa.cod_ppa = macro_objetivo.cod_ppa)
  JOIN ppa.ppa_publicacao ppa_publicacao ON (ppa_publicacao.cod_ppa = ppa.cod_ppa)
  JOIN normas.norma norma ON (norma.cod_norma = ppa_publicacao.cod_norma)
 WHERE TO_CHAR(norma.dt_publicacao, 'YYYYMM') = 'PARAMETRO_NORMA_DT_PUBLICACAO_YYYYMM'
 ORDER BY acao.cod_acao ASC
