﻿SELECT (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       cadastro.num_cadastro AS cdOperacao,
       cadastro.cod_orgao AS cdOrgao,
       TRIM(cadastro.exercicio) AS nrAnoLoa,
       TRIM(pessoa.nr_documento) AS nrCPF,
       TO_CHAR(cadastro.dt_inicio_vinculo, 'YYYY-MM-DD') AS dtInicioVinculo,
       cadastro.cod_norma AS cdControleLeiAto,
       '' AS final
  FROM tcepr.cadastro_secretario AS cadastro
  JOIN tcepr.view_pessoa_am AS pessoa ON (pessoa.numcgm = cadastro.numcgm)
 WHERE dt_baixa IS NULL
   AND TO_CHAR(cadastro.dt_inicio_vinculo, 'YYYYMM') = 'PARAMETRO_DT_INICIO_VINCULO_YYYYMM'
 ORDER BY cadastro.num_cadastro ASC
