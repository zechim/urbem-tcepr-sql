﻿SELECT (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       cadastro.num_cadastro AS cdOperacao,
       TO_CHAR(cadastro.dt_baixa, 'YYYY-MM-DD') AS dtBaixa,
       cadastro.cod_norma_baixa AS cdControleLeiAto,
       SUBSTRING(TRIM(cadastro.descricao_baixa), 1, 250) AS dsMotivo,
       '' AS final
  FROM tcepr.cadastro_secretario AS cadastro
  JOIN tcepr.view_pessoa_am AS pessoa ON (pessoa.numcgm = cadastro.numcgm)
 WHERE COALESCE(TRIM(cadastro.descricao_baixa, '')) <> '' 
   AND TO_CHAR(cadastro.dt_baixa, 'YYYYMM') = 'PARAMETRO_DT_BAIXA_YYYYMM'
   AND dt_baixa IS NOT NULL
 ORDER BY cadastro.num_cadastro ASC
