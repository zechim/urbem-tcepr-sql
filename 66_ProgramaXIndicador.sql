﻿SELECT sub.idPessoa,
       LPAD(MAX(sub.cdPrograma::TEXT), 4, '0') AS cdPrograma,
       sub.cdControleLeiAtoPrograma,
       sub.cdIndicador,
       sub.cdControleLeiAtoIndicador,
       '' AS final
  FROM ( SELECT
                (
                  SELECT TRIM(config.valor)
                    FROM administracao.configuracao config
                   WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
                     AND config.parametro = 'id_entidade_tce'
                ) AS idPessoa,
                mei.cod_programa AS cdPrograma,
                norma.cod_norma AS cdControleLeiAtoPrograma,
                mei.cod_indicador AS cdIndicador,
                mei.cod_norma_criacao AS cdControleLeiAtoIndicadorfe
           FROM tcepr.medida_esperada_indicador AS mei
           JOIN ppa.programa_indicadores indicador ON (indicador.cod_indicador = mei.cod_indicador 
            AND indicador.cod_programa = mei.cod_programa 
            AND indicador.timestamp_programa_dados = mei.timestamp_programa_dados)
           JOIN ppa.programa_dados dados ON (dados.cod_programa = indicador.cod_programa 
            AND dados.timestamp_programa_dados = indicador.timestamp_programa_dados)
           JOIN ppa.programa programa ON (programa.cod_programa = indicador.cod_programa)
           JOIN ppa.programa_setorial programa_setorial ON (programa_setorial.cod_setorial = programa.cod_setorial)
           JOIN ppa.macro_objetivo ON (macro_objetivo.cod_macro = programa_setorial.cod_macro)
           JOIN ppa.ppa_publicacao publicacao ON (publicacao.cod_ppa = macro_objetivo.cod_ppa)
           JOIN normas.norma norma ON (norma.cod_norma = publicacao.cod_norma)
          WHERE mei.dt_cancelamento IS NULL 
            AND TO_CHAR(publicacao.timestamp, 'YYYYMM') = 'PARAMETRO_PUBLICACAO_YYYYMMM'
       ) sub 
 GROUP BY sub.idPessoa, sub.cdPrograma, sub.cdControleLeiAtoPrograma, sub.cdIndicador, sub.cdControleLeiAtoIndicador
 ORDER BY sub.cdPrograma ASC
