﻿SELECT (
         SELECT TRIM(config.valor)
          FROM administracao.configuracao config
         WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
           AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       responsavel.cod_responsavel AS cdOperacao,
       pessoa.tp_documento AS tpDocumento,
       pessoa.nr_documento AS nrDocumento,
       responsavel.id_tipo_modulo AS idTipoModulo,
       TO_CHAR(responsavel.dt_inicio_vinculo, 'YYYY-MM-DD') AS dtInicioVinculo,
       '' AS final
  FROM tcepr.responsavel_modulo AS responsavel
  JOIN tcepr.view_pessoa_am AS pessoa ON (pessoa.numcgm = responsavel.numcgm)
 WHERE TO_CHAR(responsavel.dt_inicio_vinculo, 'YYYYMM') = 'PARAMETRO_DT_INICIO_VINCULO_YYYYMM'
   AND responsavel.dt_baixa IS NULL
