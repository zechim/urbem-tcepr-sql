﻿SELECT (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       LPAD(indicador.cod_indicador::TEXT, 4, '0') AS cdIndicador,
       norma.cod_norma AS cdControleLeiAto,
       '' AS final
  FROM ppa.programa_indicadores indicador
  JOIN ppa.programa_dados dados ON (dados.cod_programa = indicador.cod_programa 
   AND dados.timestamp_programa_dados = indicador.timestamp_programa_dados)
  JOIN ppa.programa programa ON (programa.cod_programa = indicador.cod_programa)
  JOIN ppa.programa_setorial programa_setorial ON (programa_setorial.cod_setorial = programa.cod_setorial)
  JOIN ppa.macro_objetivo ON (macro_objetivo.cod_macro = programa_setorial.cod_macro)
  JOIN ppa.ppa_publicacao publicacao ON (publicacao.cod_ppa = macro_objetivo.cod_ppa)
  JOIN normas.norma norma ON (norma.cod_norma = publicacao.cod_norma)
 WHERE TO_CHAR(publicacao.timestamp, 'YYYYMM') = 'PARAMETRO_PUBLICACAO_YYYYMMM' 
