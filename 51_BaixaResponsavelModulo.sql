﻿SELECT (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       responsavel.cod_responsavel AS cdOperacao,
       TO_CHAR(responsavel.dt_baixa, 'YYYY-MM-DD') AS dtBaixa,
       SUBSTRING(TRIM(responsavel.descricao_baixa), 1, 250) AS dsMotivo,
       '' AS final
  FROM tcepr.responsavel_modulo AS responsavel
  JOIN tcepr.view_pessoa_am AS pessoa ON (pessoa.numcgm = responsavel.numcgm)
 WHERE COALESCE(TRIM(responsavel.descricao_baixa, '')) <> '' 
   AND responsavel.dt_baixa IS NOT NULL
   AND TO_CHAR(responsavel.dt_baixa, 'YYYYMM') = 'PARAMETRO_DT_BAIXA_YYYYMM'
