﻿SELECT (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 1, 1) AS cdCategoriaEconomica,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 2, 1) AS cdOrigem,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 3, 1) AS cdEspecie,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 4, 1) AS cdDesdobramentoD1,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 5, 2) AS cdDesdobramentoDD2,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 7, 1) AS cdDesdobramentoD3,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 8, 1) AS cdTipoNaturezaReceita,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 9, 2) AS cdNivel8,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 11, 2) AS cdNivel9,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 13, 2) AS cdNivel10,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 15, 2) AS cdNivel11,
       SUBSTRING(REPLACE(conta_receita.cod_estrutural, '.', ''), 17, 2) AS cdNivel12,
       conta_receita.exercicio AS nrAnoAplicacao,
       conta_receita.descricao AS dsDesdobramento,
       '' AS cdTipoNivelConta,
       '' AS final
  FROM orcamento.conta_receita AS conta_receita
 WHERE conta_receita.exercicio = '2018'
 LIMIT 1
  
pegar sintetico e analitico

CASE WHEN dsTipoReceitaPadrao = 'Sintética' THEN 'S' ELSE 'A' END [cdTipoNivelConta]

select * from orcamento.conta_receita where exercicio = '2018' limit 1;


