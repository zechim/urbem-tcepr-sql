﻿SELECT (
        SELECT TRIM(config.valor)
          FROM administracao.configuracao config
         WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
           AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       sub.num_acao AS cdAcao,
       sub.norma_acao AS cdControleLeiAto,
       sub.cod_programa AS cdPrograma,
       sub.norma_programa AS cdControleLeiAtoPrograma,
       '' AS final
  FROM (
        -- PPA
        SELECT acao.num_acao, norma.cod_norma AS norma_acao, programa.cod_programa, norma.cod_norma AS norma_programa, norma.dt_publicacao
          FROM ppa.acao AS acao
          JOIN ppa.programa AS programa ON (programa.cod_programa = acao.cod_programa)
          JOIN ppa.programa_setorial AS programa_setorial ON (programa_setorial.cod_setorial = programa.cod_setorial)
          JOIN ppa.macro_objetivo AS macro_objetivo ON (macro_objetivo.cod_macro = programa_setorial.cod_macro)
          JOIN ppa.ppa AS ppa ON (ppa.cod_ppa = macro_objetivo.cod_ppa)
          JOIN ppa.ppa_publicacao AS ppa_publicacao ON (ppa_publicacao.cod_ppa = ppa.cod_ppa)
          JOIN normas.norma norma ON (norma.cod_norma = ppa_publicacao.cod_norma)
      GROUP BY acao.num_acao, norma.cod_norma, programa.cod_programa, norma.cod_norma
     UNION ALL
        -- LDO
        SELECT acao.num_acao, norma.cod_norma AS norma_acao, programa.cod_programa, norma.cod_norma AS norma_programa, norma.dt_publicacao
          FROM ldo.acao_validada AS acao_validada

          -- ppa.acao_quantidade
          JOIN ppa.acao_quantidade AS acao_quantidade ON (acao_quantidade.cod_acao = acao_validada.cod_acao
           AND acao_quantidade.timestamp_acao_dados = acao_validada.timestamp_acao_dados
           AND acao_quantidade.ano = acao_validada.ano
           AND acao_quantidade.cod_recurso = acao_validada.cod_recurso
           AND acao_quantidade.exercicio_recurso = acao_validada.exercicio_recurso)

          -- ppa.acao_recurso
          JOIN ppa.acao_recurso AS acao_recurso ON (acao_recurso.cod_acao = acao_quantidade.cod_acao
           AND acao_recurso.timestamp_acao_dados = acao_quantidade.timestamp_acao_dados
           AND acao_recurso.cod_recurso = acao_quantidade.cod_recurso
           AND acao_recurso.exercicio_recurso = acao_quantidade.exercicio_recurso
           AND acao_recurso.ano = acao_validada.ano)

          -- ppa.acao_dados
          JOIN ppa.acao_dados AS acao_dados ON (acao_dados.cod_acao = acao_recurso.cod_acao
           AND acao_dados.timestamp_acao_dados = acao_recurso.timestamp_acao_dados)

          JOIN ppa.acao AS acao ON (acao.cod_acao = acao_recurso.cod_acao)
          JOIN ppa.programa AS programa ON (programa.cod_programa = acao.cod_programa)
          JOIN ppa.programa_setorial AS programa_setorial ON (programa_setorial.cod_setorial = programa.cod_setorial)
          JOIN ppa.macro_objetivo AS macro_objetivo ON (macro_objetivo.cod_macro = programa_setorial.cod_macro)
          JOIN ppa.ppa AS ppa ON (ppa.cod_ppa = macro_objetivo.cod_ppa)
          JOIN ldo.ldo AS ldo ON (ldo.cod_ppa = ppa.cod_ppa)
          JOIN ldo.homologacao AS homologacao ON (homologacao.cod_ppa = ldo.cod_ppa AND homologacao.ano = ldo.ano)
          JOIN normas.norma norma ON (norma.cod_norma = homologacao.cod_norma)
      GROUP BY acao.num_acao, norma.cod_norma, programa.cod_programa, norma.cod_norma
       ) sub
 WHERE TO_CHAR(sub.dt_publicacao, 'YYYYMM') = 'PARAMETRO_NORMA_DT_PUBLICACAO_YYYYMM'
 ORDER BY sub.num_acao ASC, sub.norma_acao ASC, sub.cod_programa ASC
