SELECT pessoa.tp_documento AS tpDocumento,
       pessoa.nr_documento AS nrDocumento,
       pessoa.nm_pessoa AS nmPessoa,
       pessoa.ds_endereco AS dsEndereco,
       pessoa.cd_cep AS cdCEP,
       pessoa.sg_uf AS sgUF,
       '' AS final
  FROM tcepr.view_pessoa_am AS pessoa
 WHERE TO_CHAR(pessoa.timestamp_inclusao, 'YYYYMMDDHH24MISS') = 'PARAMETRO_TIMESTAMP_INCLUSAO_YYYYMMDDHH24MISS'
