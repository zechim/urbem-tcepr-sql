﻿SELECT query.tpDocumento, query.nrDocumento, query.tpOutroDocumento, query.nrOutroDocumento, '' AS final
  FROM (
         SELECT DISTINCT(pessoa.numcgm),
                pessoa.tp_documento AS tpDocumento,
                pessoa.nr_documento AS nrDocumento,
                (
                  SELECT LEFT(TRIM(valor.valor), 1)
                    FROM sw_cgm_atributo_valor valor
                    JOIN sw_atributo_cgm atributo ON (valor.cod_atributo = atributo.cod_atributo)
                    WHERE atributo.nom_atributo = 'Tipo de Documento'
                      AND valor.numcgm = pessoa.numcgm
                ) AS tpOutroDocumento,
                (
                  SELECT TRIM(valor.valor)
                    FROM sw_cgm_atributo_valor valor
                    JOIN sw_atributo_cgm atributo ON (valor.cod_atributo = atributo.cod_atributo)
                   WHERE atributo.nom_atributo = 'Número do Documento'
                     AND valor.numcgm = pessoa.numcgm
                ) AS nrOutroDocumento
           FROM tcepr.view_pessoa_am AS pessoa
           JOIN sw_cgm_atributo_valor valor ON (valor.numcgm = pessoa.numcgm) -- filter
       ) AS query
 WHERE COALESCE(query.tpOutroDocumento, '') <> '' AND COALESCE(query.nrOutroDocumento, '') <> '' 
   AND TO_CHAR(pessoa.timestamp_inclusao, 'YYYYMMDDHH24MISS') = 'PARAMETRO_TIMESTAMP_INCLUSAO_YYYYMMDDHH24MISS'
