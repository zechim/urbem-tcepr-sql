﻿SELECT (
         SELECT TRIM(config.valor)
           FROM administracao.configuracao config
          WHERE config.exercicio = 'PARAMETRO_EXERCICIO_ADM_CONFIG_ID_ENTIDADE_TCE' 
            AND config.parametro = 'id_entidade_tce'
       ) AS idPessoa,
       meom.id_objetivo_milenio AS idObjetivoMilenio,
       meom.nr_ano_aplicacao AS nrAnoAplicacao,
       meom.id_unidade_medida AS idUnidadeMedida,
       meom.nr_medida_esperada AS nrMedidaEsperada,       
       '' AS final
  FROM tcepr.medida_esperada_objetivo_milenio AS meom
 WHERE meom.nr_ano_aplicacao = 'PARAMETRO_ANO_APLICACAO_YYYY'
